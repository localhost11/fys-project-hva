# Controller program using user input to
# signal GPIO pins to controll a car.
#
# @Author Diego Brandjes
# @Class IT101-A
# @Date 19 Nov 2020

# If you use any parts of this code at a school project you agree to carrying the full responsibility 
# of any consequences from copying code. 

import keyboard
import time
import RPi.GPIO as GPIO

forwardL = 18   # Black
backwardL = 17  # White
forwardR = 19   # Gray
backwardR = 16  # Orange

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

GPIO.setup(forwardL, GPIO.OUT)
GPIO.setup(forwardR, GPIO.OUT)
GPIO.setup(backwardL, GPIO.OUT)
GPIO.setup(backwardR, GPIO.OUT)

pwmForwardLeft = GPIO.PWM(forwardL, 100)
pwmForwardRight = GPIO.PWM(forwardR, 100)

pwmBackwardLeft = GPIO.PWM(backwardL, 100)
pwmBackwardRight = GPIO.PWM(backwardR, 100)


# Speed controll and motor assignments


def startDuty(fl, fr, bl, br):
    pwmForwardLeft.start(fl)
    pwmForwardRight.start(fr)
    pwmBackwardLeft.start(bl)
    pwmBackwardRight.start(br)


def changeDuty(fl, fr, bl, br):
    pwmForwardLeft.ChangeDutyCycle(fl)
    pwmForwardRight.ChangeDutyCycle(fr)
    pwmBackwardLeft.ChangeDutyCycle(bl)
    pwmBackwardRight.ChangeDutyCycle(br)


def assignMotor(key):
    controll = key

    # turn left
    if controll == 'a' or controll == 'left':
        print('left')
        startDuty(0, 75, 0, 0)
        changeDuty(0, 75, 0, 0)

    # turn right
    if controll == 'd' or controll == 'right':
        print('right')
        startDuty(75, 0, 0, 0)
        changeDuty(75, 0, 0, 0)

    # forwards
    if controll == 'w' or controll == 'up':
        print('forwards')
        startDuty(50, 50, 0, 0)
        changeDuty(50, 50, 0, 0)

    # backwards
    if controll == 's' or controll == 'down':
        print('backwards')
        startDuty(0, 0, 50, 50)
        changeDuty(0, 0, 50, 50)


# passes on information to controller function.
def key_press(key):
    assignMotor(key.name)


keyboard.on_press(key_press)

while True:

    # calls reader function.
    if keyboard.on_press(key_press) == True:
        keyboard.on_press(key_press)
        time.sleep(1)

    # when pressing escape the program will be terminated and GPIO will be reset.
    elif keyboard.is_pressed('esc'):
        print("quit")
        break

    # sets motor values to 0 when not in use.
    else:
        print("hold")
        startDuty(0, 0, 0, 0)
        changeDuty(0, 0, 0, 0)

        time.sleep(1)

pwmForwardLeft.stop()
pwmForwardRight.stop()
pwmBackwardLeft.stop()
pwmBackwardRight.stop()
GPIO.cleanup()
